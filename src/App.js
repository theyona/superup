import React from 'react';
import './App.css';
import ColorsManagement from './Components/ColorsManagement.jsx';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ColorsManagement />
      </header>
    </div>
  );
}

export default App;
