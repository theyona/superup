const HEX_CODE_START= '#';

export function validateHexCode (input){
    if(input.startsWith(HEX_CODE_START))
        return true;
      else{
        alert('Hex code should start with #');
        return false;
    }
}
