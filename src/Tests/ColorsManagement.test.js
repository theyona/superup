import React from 'react';
import ReactDOM from 'react-dom';
import ColorsManagement from '../Components/ColorsManagement';
import enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";

enzyme.configure({ adapter: new Adapter() });

it('should add color ', () => {
  const component = enzyme.shallow(<ColorsManagement />)
  component.setState({Colors: ['#1'],NewColor:'#2'});
  component.instance().AddColor();
  expect(component.state('Colors')).toEqual(['#1','#2']);
  expect(component.state('NewColor')).toEqual('');
});

it('should remove color from list', () => {
  const component = enzyme.shallow(<ColorsManagement />)
  component.setState({Colors: ['#1','#2','#3']});
  component.instance().RemoveColor(0);
  expect(component.state('Colors')).toEqual(['#2','#3']);
});

it('should select color to edit from list', () => {
  const component = enzyme.shallow(<ColorsManagement />)
  component.setState({Colors: ['#1','#2','#3']});
  component.instance().SelectColorToEdit(0);
  expect(component.state('ColorIndexToEdit')).toEqual(0);
  expect(component.state('EditColor')).toEqual('#1');
});

it('should edit color', () => {
  const component = enzyme.shallow(<ColorsManagement />)
  component.setState({Colors: ['#1','#2','#3'], ColorIndexToEdit : 1 ,EditColor : '#22'});
  component.instance().EditColor();
  expect(component.state('Colors')).toEqual(['#1','#22','#3']);
  expect(component.state('ColorIndexToEdit')).toEqual(null); 
  expect(component.state('EditColor')).toEqual('');
});