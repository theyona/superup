import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../Style/Style.css'
import PropTypes from 'prop-types';

  function List(props) {
      let listItems = props.items.map((item,i) => {
          return <li className="list-group-item list-group-item-secondary" style= {{display : 'flex' ,minHeight: 'fit-content',backgroundColor : 'transparent',border: 'none'}} key={i} > 
                  <div style= {{flex : '2',backgroundColor : item,height: 'auto',borderRadius:'20px'}} >{}</div> 
                  <button className='btn btn-outline-info Button' onClick={() =>  props.onClickEdit(i)} >Edit</button>
                  <button className='btn btn-outline-danger Button' onClick={() =>  props.onClickRemove(i)} >Remove</button>
                  </li>}
      );
    return (
      <div className="Center">
          <ul className="list-group List">
          {listItems}
          </ul>
      </div>
      );
  }

  List.propTypes = {
    items: PropTypes.array,
    onClickEdit: PropTypes.func,
    onClickRemove: PropTypes.func
  };

  export default List