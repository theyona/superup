import React, {Component } from 'react';
import List from './List';
import '../Style/Style.css'
import {validateHexCode} from '../Connons.js'

class ColorsManagement extends Component {
    constructor(props){
        super(props)
        this.state = { 
          ColorIndexToEdit : null,
          Colors : [],
          NewColor: '',
          EditColor: ''
        }
    }
    
    onChangeEditColor = (event) =>{
      this.setState({EditColor : event.target.value})
    }
    onChangeAddColor = (event) =>{
      this.setState({NewColor : event.target.value})
    }
    AddColor = () =>{
      
      if(validateHexCode(this.state.NewColor)){
        let newColors = this.state.Colors.concat([this.state.NewColor]);
        this.setState({Colors : newColors,NewColor : ""});
      }
    }
    RemoveColor = (ColorIndex) =>{
      let newColors = this.state.Colors;
      newColors.splice(ColorIndex, 1);
      this.setState({Colors : newColors});
    } 
    SelectColorToEdit = (ColorIndex)=> {
      const color = this.state.Colors[ColorIndex];
      this.setState({ColorIndexToEdit : ColorIndex,EditColor : color});
    }
    EditColor = () => {
      if(validateHexCode(this.state.EditColor)){
        let newColors = this.state.Colors;
        newColors[this.state.ColorIndexToEdit] = this.state.EditColor;
        this.setState({Colors : newColors, ColorIndexToEdit : null, EditColor : ""});
      }

    }
    render() {
      return (
        <div className="Main">
          <p className="Center">Color Managment</p>
          
          <List items = {this.state.Colors} onClickEdit = {this.SelectColorToEdit} onClickRemove={this.RemoveColor}/>

          {this.state.ColorIndexToEdit !== null && 
            <div className="Center" style= {{display : 'flex',height:'60%'}}>
            <input type="text" id="TxtEditColor" className="form-control" style= {{flex : '4'}} 
            aria-label="Default" aria-describedby="inputGroup-sizing-default" 
            onChange={this.onChangeEditColor} value={this.state.EditColor}/>
            <button className='btn btn-outline-info Button' style= {{flex : '1'}}  onClick={this.EditColor} >Save</button>
            </div>
          }
          <div className="Center" style= {{display : 'flex',height:'60%'}}>
            <input type="text" id="TxtAddColor" className="form-control" style= {{flex : '4'}} 
            aria-label="Default" aria-describedby="inputGroup-sizing-default"
            onChange={this.onChangeAddColor} value={this.state.NewColor}/>
            <button id="btnAdd" className='btn btn-outline-success Button' style= {{flex : '1'}} onClick={this.AddColor} >Add</button>
          </div>
        </div>
        );
    }
  }

export default ColorsManagement